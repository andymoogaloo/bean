**Bean.**

Just another responsive startup framework to get things moving.

Built on SCSS, the Guardian @mq mixin, some old 320andUp styles and a few common JS files.

Andy @moogaloo