
// creates the various page functions
function init() {
	$('.slick-container').slick({
	//	  adaptiveHeight: true,
		  fade: true,
	//	  variableWidth: true,
		  dots: false,
		  arrows: false,
		  autoplay: true,
		  slidesToShow: 1,
		  pauseOnHover: false,
		  autoplaySpeed: 2500,
		  speed: 2500,
		  easing: 'easeInOut'
	});
	
	
	$( ".section-tabs" ).tabs({
		hide: { effect: "fade", duration: 800, easing: "easeInOutQuint" },
		show: { effect: "fade", duration: 800, easing: "easeInOutQuint" }
	});
		
	//opens and closes navigation on nav menu button
	$('.page-header .menu-trigger').click(function() {
		$('.page-header').toggleClass('show-nav');
		return false;
	});
	
	$('form label.radio').click(function() {
		$('form .selected').removeClass('selected');
		$(this).addClass('selected');
	});
	

	$( ".accordion" ).accordion({
		collapsible: true,
		active: false,
		heightStyle: "content"
	});
	
	$( ".programme-sections" ).accordion({
		collapsible: true,
		active: 0,
		heightStyle: "content"
	});
	  
  

  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'') && location.hostname === this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
      }
    }
   	return false;
  });

  	// remove orphaned words from main text content 
  	var contentText = '.centre-text > p';
	$(contentText).each(function(){
	    var string = $(this).html();
	    string = string.replace(/ ([^ ]*)$/,'&nbsp;$1');
	    $(this).html(string);
	});

	
//	$('#main').addClass('is-loaded');
} 

// initialises the various page functions on page load
$(document).ready(function(){
  	init();
});


// Contents of functions.js
/*
$(function(){
  'use strict';
  var options = {
    prefetch: true,
    cacheLength: 2,
    anchors: 'a' ,
    onStart: {
      duration: 900, // Duration of our animation
      render: function ($container) {
        // Add your CSS animation reversing class
        $container.addClass('is-exiting');

        // Restart your animation
        smoothState.restartCSSAnimations();
      }
    },
    onReady: {
      duration: 0,
      render: function ($container, $newContent) {
        // Remove your CSS animation reversing class
        $container.removeClass('is-exiting');
        $container.addClass('is-loaded');

        // Inject the new content
        $container.html($newContent);

      }
    },
	onProgress: {
	    // How long this animation takes
	    duration: 0,
	    // A function that dictates the animations that take place
	    render: function ($container) {
	        $container.addClass('is-loading');	    
	    }
	},
	
    // initialises page functions after smoothState loaded
    onAfter: function() {
		init();
    }
  },
  smoothState = $('#main').smoothState(options).data('smoothState');
});

*/



// Closes Navigation on local select
$('.js-click .jump-link').click(function() {
	$('.show-nav').toggleClass('show-nav');
	return false;
});

/* sets general cookies
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires + ";path=/;";
}

// Hides message and sets message cookie
$('.flash-message .trigger').click(function() {
	$('.flash-message').toggleClass('hide-block');
	setCookie('message', 'agreed', 1000);
	return false;
});

// Sets language cookie and reloads page
$('.page-header-row .lang-trigger').click(function() {
	var lang = $(this).data('lang');
	setCookie('language', lang, 1000);
	location.reload();
	return false;
});
*/


$('.js-move .trigger').click(function() {
	$(this).parent().toggleClass('move');
});
$('.js-drop-once .trigger').click(function() {
	$(this).parent().addClass('drop');
});








$(function() {
});

/*

// Fancybox forms

$(document).ready(function() {

	$(".preview-slide").fancybox({
		'speedIn':300,
		'speedOut':300,
		'margin':20,
		'padding':0,
		'autoDimensions': true,
		'hideOnOverlayClick':true,
		'overlayShow':true,
		'overlayColor':'#222',
		'overlayOpacity':0.8,
		'titleShow':false
	});
	
	$(".overlay").fancybox({
		'speedIn':300,
		'speedOut':300,
		'padding':0,
		'hideOnOverlayClick':true,
		'overlayShow':true,
		'overlayColor':'#fff',
		'overlayOpacity':0.8,
		'titlePosition':'over'
	});
	
	
	
	
});

*/
